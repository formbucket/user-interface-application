import { h } from "preact";
import { connect } from "unistore/preact";
import GuideWebhooks from "../components/GuideWebhooks";
import Layout from "../components/Layout";
import { actions } from "../src/store";

let Page = connect(
  "error,menuOn,flash,user",
  actions
)(props => (
  <Layout shouldLoadUser={true} {...props}>
    <GuideWebhooks />
  </Layout>
));
export default Page;
